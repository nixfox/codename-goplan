package main

import (
	"encoding/json"
	"fmt"
	_ "net"
	"net/http"
	"pman/api"
	"pman/conf"
	"pman/db"
	"pman/logger"
	"pman/model"
	"strings"
	"time"
)

var user *model.User
var proj *model.Project
var usess *model.User_session
var dbase *database.Database
var cnf *conf.Configuration
var pmanAPI *api.API
var router *api.Router

func main() {
	router := new(api.Router).New_Router("default", 0, "/")
	router.Add_Route("/user")
	router.Add_Route("/project")

	cnf, err := new(conf.Configuration).Get_config("conf/pman.conf")
	if err != nil {
		logger.Print_error(err)
	}
	pmanAPI = new(api.API)
	proj = new(model.Project)
	user = new(model.User)
	dbase = new(database.Database)
	usess = new(model.User_session)
	dbase.Create_database(cnf.DB_driver, cnf.Sqlite3_location)
	dbase.Create_table(usess)
	usess.Create_session("nixfox", "192.168.0.55", "linux", "chrome", dbase)
	usess.Set_offline("Q60A5ivZvNJNo725-5sECMyRcFOey2PN04ku_Xkj97w=", dbase)
	go usess.Clean_sessions(cnf.Clear_interval, dbase)
	mux := http.NewServeMux()
	mux.HandleFunc("/", home_handle)
	mux.HandleFunc("/user/", user_handler)
	mux.HandleFunc("/project/", project_handler)
	http.ListenAndServe(cnf.Listen, mux)
}
func home_handle(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		fmt.Fprintf(w, "<h1>GoPlan</h1> %s!", r.URL.Path[1:])
	} else if r.Method == "POST" {
		fmt.Fprintf(w, "POST HAPPENED %s %s", r.FormValue("userid"), r.Header.Get("Content-Type"))
	}
	fmt.Printf("request recieved [%s] %s %s\n", time.Now().Format(time.RFC1123), r.Method, r.RequestURI)
}
func user_handler(w http.ResponseWriter, r *http.Request) {
	url_arr := delete_empty(pmanAPI.Split_URL(r.URL.String()))
	pmanAPI.Create_response(w, r)
	fmt.Fprint(w, pmanAPI.Get_IP(), pmanAPI.Get_Browser(), pmanAPI.Get_OS())
	fmt.Println(url_arr)
}
func project_handler(w http.ResponseWriter, r *http.Request) {
	urlArray := strings.Split(r.URL.Path, "/")
	urlArray = delete_empty(urlArray)
	uname := urlArray[2]
	pname := urlArray[1]
	chk, err := proj.Check_exists(pname, uname, dbase)
	if err != nil {
		logger.Print_error(err)
	}
	if chk == true {
		// project exists
		if r.Method == "GET" {
			if r.URL.RawQuery == "xml" {
				xml, err := proj.Project_XML(pname, uname, dbase)
				if err != nil {
					logger.Print_error(err)
				}
				fmt.Fprintf(w, "%s", xml)
			} else {
				json, err := proj.Project_JSON(pname, uname, dbase)
				if err != nil {
					logger.Print_error(err)
				}
				fmt.Fprintf(w, "%s", json)
			}
		} else if r.Method == "POST" {
			if r.Header.Get("Content-Type") == "application/json" {
				decoder := json.NewDecoder(r.Body)
				err := decoder.Decode(proj)
				if err != nil {
					logger.Print_error(err)
				}
				proj.Insert_project(proj.Name, proj.Description, proj.Author, proj.Completion_date, dbase)
			} else if r.Header.Get("Content-Type") == "text/xml" {
			}
		}
	} else {
		// project does not exist
		fmt.Fprintf(w, "the project:'%s' does not exist under this username:'%s'", pname, uname)
	}
}
func delete_empty(s []string) []string {
	var r []string
	for _, str := range s {
		if str != "" {
			r = append(r, str)
		}
	}
	return r
}
