package logger

import (
	"errors"
	_ "fmt"
	"os"
	"path"
	"runtime"
	"time"
)

func Log_file(fPath string, str string) {
	dir, _ := path.Split(fPath)
	if !check_exists(fPath) {
		err := os.MkdirAll(dir, 0775)
		if err != nil {
			Print_error(err)
		}
	} else {

		// Stat the file path
		stat, err := os.Stat(fPath)
		if err != nil {
			Print_error(err)
		}
		// check if log file is greater than 5Megabytes
		if stat.Size() >= 5000000 {
			// if FileSize is greater than 5Megabytes rename it to FileName.old
			os.Rename(fPath, fPath+".old")
			// create a new file and append 1
			fPath += "1"
		}
	}
	f, err := os.OpenFile(fPath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0775)
	if err != nil {
		Print_error(err)
	}
	str = "\n" + string(log_precision([]byte(str), 1))
	_, err = f.WriteString(str)
	//fmt.Printf("\nWrote to %s length: %v", filestr, n)
	if err != nil {
		Print_error(err)
	}
	f.Close()
}
func log_precision(logMsg []byte, precision int) []byte {
	if precision == 0 {
		logMsg = logMsg
	} else if precision == 1 {
		logMsg = append_time(logMsg)
	}
	return logMsg
}
func append_time(msg []byte) []byte {
	timeStr := time.Now().Format(time.RFC1123)
	msg = []byte("[" + timeStr + "] " + string(msg))
	return msg
}

func check_exists(dPath string) bool {
	if _, err := os.Stat(dPath); os.IsNotExist(err) {
		return false
	}
	return true
}
func get_os() (string, error) {
	if runtime.GOOS == "linux" {
		return "LINUX", nil
	}
	if runtime.GOOS == "windows" {
		return "SHIT", nil
	}
	if runtime.GOOS == "darwin" {
		return "OSX", nil
	}
	if runtime.GOOS == "freebsd" {
		return "BSD", nil
	}
	return "", errors.New("unknown or unsupported OS.")
}
