package api

import (
	"net"
	"net/http"
	"pman/db"
	"pman/model"
	"strings"
)

type API struct {
	Response_writer http.ResponseWriter
	Request         *http.Request
}

func (api *API) Create_response(w http.ResponseWriter, r *http.Request) *API {
	api.Response_writer = w
	api.Request = r
	return api
}
func (api *API) Redirect(condition bool, redirect_uri string, code int) {
	if code == 0 {
		code = 301
	}
	if condition {
		http.Redirect(api.Response_writer, api.Request, redirect_uri, code)
	}
}
func (api *API) Login_user(usr *model.User, usess *model.User_session, base *database.Database) (bool, error) {
	chk, err := usr.User_login(usr.Username, usr.Password, base)
	if err != nil {
		return false, err
	}
	if chk {
		ip := api.Get_IP()
		os := api.Get_OS()
		browser := api.Get_Browser()
		check_session, err := usess.Create_session(usr.Username, ip, os, browser, base)
		if err != nil {
			return false, err
		}
		if check_session {
			return true, nil
		}
	}
	return false, err
}
func (api *API) Get_IP() string {
	ip_1 := api.Request.Header.Get("X-FORWARDED-FOR")
	ip_2, _, _ := net.SplitHostPort(api.Request.RemoteAddr)
	if ip_1 == ip_2 {
		return ip_1
	}
	if ip_2 == "::1" {
		ip_2 = "127.0.0.1"
	}
	return ip_2
}
func (api *API) Get_OS() string {
	user_agent := api.Request.UserAgent()
	if strings.Contains(strings.ToLower(user_agent), "linux") {
		return "linux"
	} else if strings.Contains(strings.ToLower(user_agent), "windows") {
		return "windows"
	} else if strings.Contains(strings.ToLower(user_agent), "mac os x") {
		return "osx"
	}
	return "other"
}
func (api *API) Get_Browser() string {
	user_agent := api.Request.UserAgent()
	if strings.Contains(strings.ToLower(user_agent), "chrome") {
		return "chrome"
	} else if strings.Contains(strings.ToLower(user_agent), "firefox") {
		return "firefox"
	} else if strings.Contains(strings.ToLower(user_agent), "safari") {
		return "safari"
	} else if strings.Contains(strings.ToLower(user_agent), "opera") {
		return "opera"
	} else if strings.Contains(strings.ToLower(user_agent), "MSIE") {
		return "crap"
	}
	return "other"
}
func (api *API) Split_URL(url string) []string {
	url_arr := strings.Split(url, "/")
	return url_arr
}
