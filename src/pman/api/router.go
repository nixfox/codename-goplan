package api

import "fmt"

type Router struct {
	Name   string
	Id     int
	Routes []string
}

func (router *Router) New_Router(name string, id int, root_route string) *Router {
	router.Id = id
	router.Name = name
	router.Routes = append(router.Routes, root_route)
	return router
}
func (router *Router) Add_Route(path string) {
	router.Routes = append(router.Routes, path)
}
func (router *Router) Routes_List() []string {
	return router.Routes
}
func (router *Router) Route_Exists(path string) bool {
	for _, element := range router.Routes {
		if element == path {
			return true
			break
		}
	}
	return false
}
func (router *Router) Route_Index(path string) (error, int) {
	i := 0
	var err error
	for index, element := range router.Routes {
		if element == path {
			i = index
			err = nil
			break
		} else {
			err = fmt.Errorf("the route '%s' could not be found or does not exist.", path)
		}
	}
	return err, i
}
func (router *Router) Delete_Route(path string) (error, bool) {
	check := router.Route_Exists(path)
	if check == true {
		err, i := router.Route_Index(path)
		if err != nil {
			return err, false
		}
		router.Routes = append(router.Routes[:i], router.Routes[i+1:]...)
	}
	return fmt.Errorf("the route '%s' does not exist and could not be deleted", path), false
}
