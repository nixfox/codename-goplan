package conf

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
	_ "os"
	"time"
)

type Configuration struct {
	Name             string
	Author           string
	Version          float32
	Description      string
	Contributors     []string
	Error_log        string
	Sql_log          string
	Access_log       string
	Logging          bool
	Logging_level    int
	Listen           string
	DB_driver        string
	Sqlite3_location string
	Clear_interval   time.Duration
}

func (*Configuration) Get_config(fpath string) (*Configuration, error) {
	config := new(Configuration)
	file, err := ioutil.ReadFile(fpath)
	if err != nil {
		return nil, err
	}
	yaml.Unmarshal(file, config)
	return config, nil
}
