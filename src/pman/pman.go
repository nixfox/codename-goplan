package pman

import "pman/conf"
import "fmt"

func INFO() {
	cfg, _ := conf.Get_config("conf/pman.conf")
	fmt.Println("\n--------------------------------")
	fmt.Println("NAME: ", cfg.Name)
	fmt.Println("--------------------------------")
	fmt.Printf("AUTHOR: %s\nVERSION: %v\nDESCRIPTION: %s",
		cfg.Author,
		cfg.Version,
		cfg.Description)
}
