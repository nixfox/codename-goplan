package model

import "fmt"

func Update_field(table string, field string, value string, condition string) string {
	return fmt.Sprintf("UPDATE %s SET %s='%s' WHERE %s", table, field, value, condition)
}
func Select_from(table string, values string, condition string) string {
	return fmt.Sprintf("SELECT %s FROM %s WHERE %s", values, table, condition)
}
