package model

import (
	"time"
)

type Task struct {
	Name            string `notnull default:'Awesome task'`
	Description     string `default:'Description empty!'`
	Author          string `foreignkey:User.Username`
	Completion_date time.Time
	Complete        bool `default:false`
}
