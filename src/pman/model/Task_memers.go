package model

type Task_member struct {
	Id       int    `key:primary notnull`
	User     string `foreignkey:User.Username`
	Task     int    `foreignkey:Task.Id`
	Project  int    `foreignkey:Project.Id`
	complete bool   `default:false`
}
