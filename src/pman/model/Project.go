package model

import (
	"crypto/sha1"
	"encoding/base64"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"pman/db"
	"time"
)

type Project struct {
	Id              int       `key:primary`                       // primary key of User table, allows us to sort through the table
	Name            string    `notnull default:'Awesome project'` // Name of the project can not be null and is not unique as multiple projects can share the same name
	Description     string    `default:'Description empty!'`      // description of the project, a short text describing the project
	Author          string    `foreignkey:User.Username`          // author of the project
	Completion_date time.Time ``                                  // completion date of the project holds the projects deadline
	Complete        bool      `default:false`                     // bools value if project has been completed or not (true,false)
	Members         int       `default:1`                         // members working on the project default: 1(the author)
	Project_sha     string    `unique`                            // combination of the projects name and author hashed with sha1 algo
}
type Project_public struct {
	Name            string
	Description     string
	Author          string
	Completion_date time.Time
	Complete        bool
	Members         int
}

func (project *Project) Insert_project(name string, description string, author string, completion_date time.Time, base *database.Database) (string, error) {
	hasher := sha1.New()
	hasher.Write([]byte(name + author))
	sha := base64.URLEncoding.EncodeToString(hasher.Sum(nil))
	query := fmt.Sprintf("INSERT INTO Project(Name, Description,Author, Completion_date,project_sha) VALUES('%s','%s','%s','%s','%s')", name, description, author, completion_date, sha)
	err := base.Run_query(query)
	if err != nil {
		return "", err
	}
	return query, nil
}
func (project *Project) Delete_project(name string, author string, base *database.Database) (string, error) {
	query := fmt.Sprintf("DELETE FROM Project WHERE Name='%s' AND Author='%s'", name, author)
	err := base.Run_query(query)
	if err != nil {
		return "", err
	}
	return query, nil
}

func (project *Project) Get_project(name string, author string, base *database.Database) (*Project, error) {
	query := fmt.Sprintf("SELECT Name,Description,Author,Completion_date,Complete,Members,Project_sha FROM Project WHERE Name='%s' AND Author='%s'", name, author)
	stmt, err := base.DB.Prepare(query)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()
	rows, err := stmt.Query()
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		err := rows.Scan(&project.Name, &project.Description, &project.Author, &project.Completion_date, &project.Complete, &project.Members, &project.Project_sha)
		if err != nil {
			return nil, err
		}
	}
	return project, nil
}

func Update_project_field(field string, value string, condition string, base *database.Database) (string, error) {
	query := fmt.Sprintf("UPDATE Project SET %s='%s' WHERE %s", field, value, condition)
	err := base.Run_query(query)
	if err != nil {
		return "", err
	}
	return query, nil
}
func (project *Project) Project_JSON(name string, author string, base *database.Database) (string, error) {
	project, err := project.Get_project(name, author, base)
	if err != nil {
		return "", err
	}
	b, err := json.Marshal(project)
	if err != nil {
		return "", err
	}
	return string(b), nil
}
func (project *Project) Project_XML(name string, author string, base *database.Database) (string, error) {
	project, err := project.Get_project(name, author, base)
	if err != nil {
		return "", err
	}
	b, err := xml.Marshal(project)
	if err != nil {
		return "", err
	}
	return string(b), nil
}
func (project *Project) Check_exists(name string, author string, base *database.Database) (bool, error) {
	query := fmt.Sprintf("SELECT * FROM Project WHERE Name='%s' AND Author='%s'", name, author)
	stmt, err := base.DB.Prepare(query)
	defer stmt.Close()
	if err != nil {
		return false, err
	}
	rows, err := stmt.Query()
	defer rows.Close()
	if err != nil {
		return false, err
	}
	i := 0
	for rows.Next() {
		i++
	}
	if i == 0 {
		return false, nil
	}
	return true, nil
}
