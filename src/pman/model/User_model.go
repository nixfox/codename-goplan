//Package model holds database models as structs to be reflected into the database
package model

import (
	"crypto/sha1"
	"encoding/base64"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"golang.org/x/crypto/bcrypt"
	"pman/db"
	"time"
)

// Private user struct, this is what we create the database table with, it's reflects all the columns in the table
type User struct {
	Id          int       `key:primary`                                    // primary key by which we can sort through our database
	Username    string    `unique`                                         // username of the registered user, has to be unique
	Password    string    `notnull`                                        // password of the user in bcrypt hash format
	Email       string    `unique notnull`                                 // email of the user, has to be unique
	Verified    bool      `default:false`                                  // tells us if the user's email has been verified or not, defaults to false
	User_group  string    `foreignkey:User_group.Name, default:'user'`     // the privilege group the user belongs to, foreign key of User_group.Name
	Online      bool      `default:false`                                  // tells us if the user is online or not
	Last_online time.Time `default:null`                                   // tells us the last time the user has been online
	Profile_pic string    `default:'/static/jpg/profile_pics/profile.jpg'` // the location of the users profile pic
	User_sha    string    `null`                                           // combination of username email and password
}

// Public user struct, this is what we display to the client making a http request to our api
type User_public struct {
	Username    string
	Email       string
	Verified    string
	User_group  string
	Online      bool
	Last_online time.Time
	Profile_pic string
}

// Allows us to insert a new user into our database
// accepts parameters of username, email and password if the parameter of user_group is "" then it will default to user
// the base parameter is of type *database.Database, and it tells us in which database we'll be inserting our user
// this allows for reusing the same function for multiple databases, if our API needs that
func (*User) Insert_user(username string, email string, password string, user_group string, base *database.Database) (string, error) {
	if user_group == "" {
		user_group = "user"
	}
	hpass, err := hash_pass(password)
	if err != nil {
		return "", err
	}
	hasher := sha1.New()
	hasher.Write([]byte(username + email + password))
	sha := base64.URLEncoding.EncodeToString(hasher.Sum(nil))
	query := fmt.Sprintf("INSERT INTO User(Username,Email,Password,User_group,User_sha) VALUES('%s','%s','%s','%s','%s')", username, email, hpass, user_group, sha)
	err = base.Run_query(query)
	return query, err
}

// Deletes the user by their username
// The parameter username is of type string and tells the function which user we wish to delete,
// as usernames have the unique constraint it'll only delete the one user
// the parameter base if of type *database.Database and it tells us which database we'll be deleting the user from
func (usr *User) Delete_user(username string, base *database.Database) (string, error) {
	query := fmt.Sprintf("DELETE FROM USER WHERE Username='%s'", username)
	err := base.Run_query(query)
	if err != nil {
		return "", err
	}
	return query, nil
}

// Get public user struct by username
// parameter username is of type string and tells us which user we wish to retrieve the data for
// base if of type *database.Database and tells us which database we wish to retrieve the user from
// the function returns *User_public which is a type of struct
func (usr *User) Get_user(username string, base *database.Database) (*User_public, error) {
	pub_user := new(User_public)
	query := fmt.Sprintf("SELECT Username,Email,Verified,Online,Profile_pic,User_group FROM User where Username='%s'", username)
	stmt, err := base.DB.Prepare(query)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()
	rows, err := stmt.Query()
	if err != nil {
		return nil, err
	}
	err = rows.Err()
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		err := rows.Scan(&pub_user.Username, &pub_user.Email, &pub_user.Verified, &pub_user.Online, &pub_user.Profile_pic, &pub_user.User_group)
		if err != nil {
			return nil, err
		}
	}
	return pub_user, nil
}

// Changes the username for the target user
// username is of type string and it tells us which username we wish to change
// newUname is of type string and it holds the value of the new username
// base is of type *database.Database and it tells us in which database we wish to change the username of the user
func (usr *User) Change_username(username string, newUname string, base *database.Database) (string, error) {
	query := fmt.Sprintf("UPDATE User SET Username='%s' WHERE Username='%s'", newUname, username)
	err := base.Run_query(query)
	if err != nil {
		return "", nil
	}
	return query, nil
}

// Changes the email of the target user
// email is of type string and it tells us which email to change
// newEmail is of type string and it holds the value of the new email
// base if of type *database.Database and it tells us in which database we wish to change the user's email in
func (usr *User) Change_email(email string, newEmail string, base *database.Database) (string, error) {
	query := fmt.Sprintf("UPDATE User SET Email='%s' WHERE Email='%s'", newEmail, email)
	err := base.Run_query(query)
	if err != nil {
		return "", err
	}
	return query, nil
}

// changes the users password
// username is of type string and it tells us for which user're we are changing the password
// newPassword is of type string and it holds the value of the new password
// base is of type *database.Database and it tells us in which database we wish to change the target user's password
func (usr *User) Change_password(username string, newPassword string, base *database.Database) (string, error) {
	hpass, err := hash_pass(newPassword)
	if err != nil {
		return "", err
	}
	query := fmt.Sprintf("UPDATE User SET Password='%s' WHERE Username='%s'", hpass, username)
	err = base.Run_query(query)
	if err != nil {
		return "", err
	}
	return query, nil
}

// retrieves the user's sha value
// username is of type string and it tells us the target user's username
// base is of type *database.Database and it tells us from which database we'll be retrieving the info
func (usr *User) Get_user_sha(username string, base *database.Database) (string, error) {
	query := fmt.Sprintf("SELECT User_sha FROM User where Username='%s'", username)
	stmt, err := base.DB.Prepare(query)
	if err != nil {
		return "", err
	}
	defer stmt.Close()
	rows, err := stmt.Query()
	if err != nil {
		return "", err
	}
	defer rows.Close()
	for rows.Next() {
		err := rows.Scan(&usr.User_sha)
		if err != nil {
			return "", err
		}
	}
	return usr.User_sha, nil
}

// checks weather the target user exists
// username is of type string and it tells us the target user
// base if of type *database.Database and it tells us which database we'll be retrieving the info from
// returns bool and error
// bool will be true if user exists and error will be nil if no errors happened
// bool will be false if user does not exist and error will be nil if no errors happened
// bool will be false and error will hold the error value if there was an error
func (*User) Check_exists(username string, base *database.Database) (bool, error) {
	query := fmt.Sprintf("SELECT * FROM User WHERE Username='%s'", username)
	stmt, err := base.DB.Prepare(query)
	if err != nil {
		return false, err
	}
	rows, err := stmt.Query()
	if err != nil {
		return false, err
	}
	i := 0
	for rows.Next() {
		i++
	}
	if i > 0 {
		return true, nil
	}
	return false, nil
}

// retrieves the User_public struct as a json string
func (usr *User) User_JSON(username string, base *database.Database) (string, error) {
	user, err := usr.Get_user(username, base)
	if err != nil {
		return "", err
	}
	b, err := json.Marshal(user)
	if err != nil {
		return "", err
	}

	return string(b), nil
}

// retrieves the User_public struct in an XML format
func (usr *User) User_XML(username string, base *database.Database) (string, error) {
	user, err := usr.Get_user(username, base)
	if err != nil {
		return "", err
	}
	b, err := xml.Marshal(user)
	if err != nil {
		return "", err
	}
	return string(b), nil
}

// checks the user's supplied credentials and returns either true if login is possible or false if login is not possible
func (usr *User) User_login(username string, password string, base *database.Database) (bool, error) {
	query := fmt.Sprintf("SELECT Username,Password FROM User WHERE Username='%s'", username)
	stmt, err := base.DB.Prepare(query)
	defer stmt.Close()
	if err != nil {
		return false, err
	}
	rows, err := stmt.Query()
	defer rows.Close()
	if err != nil {
		return false, err
	}
	for rows.Next() {
		err := rows.Scan(&usr.Username, &usr.Password)
		if err != nil {
			return false, err
		}
	}
	err = bcrypt.CompareHashAndPassword([]byte(usr.Password), []byte(password))
	if err != nil {
		return false, err
	}
	return true, nil
}

// private function that takes care of password hashing
func hash_pass(password string) (string, error) {
	cost := 10
	pass, err := bcrypt.GenerateFromPassword([]byte(password), cost)
	if err != nil {
		return "", err
	}
	return string(pass), nil
}
