package model

type Project_member struct {
	Id         int    `key:primary notnull`
	Project    int    `foreignkey:Project.Id`
	User       string `foreignkey:User.Username`
	User_group string `foreignkey:User_group.Name`
}
