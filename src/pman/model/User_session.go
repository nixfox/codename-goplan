package model

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"pman/db"
	"time"
)

type User_session struct {
	Id          int  `key:primary`
	Online      bool `default:true`
	Last_online time.Time
	Session_key int    `unique`
	User_name   string `foreignkey:User.username, unique`
	Ip          string `notnull`
	OS          string `notnull`
	Browser     string `notnull`
}

func (Usess *User_session) Create_session(username string, ip string, os string, browser string, base *database.Database) (bool, error) {
	skey_h := make([]byte, 32)
	_, err := rand.Read(skey_h)
	if err != nil {
		return false, err
	}
	skey := base64.URLEncoding.EncodeToString([]byte(skey_h))
	query := fmt.Sprintf("INSERT INTO User_session(Online,Last_online,Session_key,User_name,Ip,OS,Browser) VALUES('%v','%v','%s','%s','%s','%s','%s')", true, time.Now(), skey, username, ip, os, browser)
	err = base.Run_query(query)
	if err != nil {
		return false, err
	}
	return true, nil
}
func (Usess *User_session) Get_session(username string, base *database.Database) (*User_session, error) {
	query := fmt.Sprintf("SELECT Online,Last_online,Session_key,User_name,Ip,OS,Browser FROM User_session WHERE User_name='%s", username)
	stmt, err := base.DB.Prepare(query)
	defer stmt.Close()
	if err != nil {
		return nil, err
	}
	rows, err := stmt.Query()
	defer rows.Close()
	for rows.Next() {
		err := rows.Scan(&Usess.Online, &Usess.Last_online, &Usess.Session_key, &Usess.User_name, &Usess.Ip, &Usess.OS, &Usess.Browser)
		if err != nil {
			return nil, err
		}
	}
	return Usess, nil
}
func (Usess *User_session) Delete_session_username(username string, base *database.Database) (bool, error) {
	query := fmt.Sprintf("DELETE FROM User_session WHERE User_name='%s'", username)
	err := base.Run_query(query)
	if err != nil {
		return false, err
	}
	return true, nil
}
func (Usess *User_session) Delete_session(session_key string, base *database.Database) (bool, error) {
	query := fmt.Sprintf("DELETE FROM User_session WHERE Session_key='%s'", session_key)
	err := base.Run_query(query)
	if err != nil {
		return false, err
	}
	return true, nil
}
func (Usess *User_session) Set_offline(session_key string, base *database.Database) (bool, error) {
	query := fmt.Sprintf("UPDATE User_session SET Online='false' WHERE Session_key='%s'", session_key)
	err := base.Run_query(query)
	if err != nil {
		return false, err
	}
	return true, nil
}
func (Usess *User_session) Set_online(session_key string, base *database.Database) (bool, error) {
	query := fmt.Sprintf("UPDATE User_session SET Online='true' WHERE Session_key='%s'", session_key)
	err := base.Run_query(query)
	if err != nil {
		return false, err
	}
	return true, nil
}
func (Usess *User_session) Clean_sessions(ms time.Duration, base *database.Database) (int64, error) {
	query := fmt.Sprintf("DELETE FROM User_session WHERE Online='false'")
	stmt, err := base.DB.Prepare(query)
	var i int64
	if err != nil {
		return 0, err
	}
	if ms != 0 {
		ticker := time.NewTicker(time.Millisecond * ms)
		go func() {
			for t := range ticker.C {
				result, err := stmt.Exec()
				if err != nil {
					fmt.Println(err)
					break
				}
				i, err = result.RowsAffected()
				if err != nil {
					fmt.Println(err)
					break
				}
				if i > 0 {
					fmt.Printf("Cleared %d rows at: %s\n", i, t)
				}
				defer stmt.Close()
			}
		}()
	}
	return i, nil
}
func (Usess *User_session) Refresh_token(token string, base *database.Database) error {
	query := fmt.Sprintf("UPDATE User_session SET Last_online='%s' where Session_key='%s'", time.Now(), token)
	err := base.Run_query(query)
	if err != nil {
		return err
	}
	return nil
}
