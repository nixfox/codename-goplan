package model

type User_group struct {
	Id          string `key:primary`
	Name        string `unique`
	Priv_level  int    `default:100`
	Description string
}
