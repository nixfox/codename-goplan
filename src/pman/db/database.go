package database

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"pman/logger"
	"reflect"
	"strings"
)

type Database struct {
	Type        string
	Location    string
	Transaction *sql.Tx
	DB          *sql.DB
	err         error
}

func (base *Database) Create_database(Type string, Location string) *Database {
	base.Location = Location
	base.Type = Type
	base.DB, base.err = sql.Open(base.Type, base.Location)
	logger.Print_error(base.err)
	ping_db(base.DB)
	begin_db(base.DB, base)
	return base
}
func (base *Database) Run_query(query string) error {
	stmt, err := base.DB.Prepare(query)
	if err != nil {
		return err
	}
	result, err := stmt.Exec()
	if err != nil {
		return err
	}
	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return err
	}
	lastID, err := result.LastInsertId()
	if err != nil {
		return err
	}
	fmt.Printf("query succesful\n rows affected : %v \n last id : %v\n", rowsAffected, lastID)
	stmt.Close()
	return nil
}
func (base *Database) Create_table(val interface{}) (string, error) {
	name := reflect.Indirect(reflect.ValueOf(val)).Type().Name()
	v := reflect.ValueOf(val).Elem()

	var foreignkey, defaultval, unique, nullable, keys string
	query := "CREATE TABLE " + name + " ("
	for i := 0; i < v.NumField(); i++ {
		fld := v.Type().Field(i)
		if strings.Contains(string(fld.Tag), ",") {
			marr := strings.Split(string(fld.Tag), ",")
			for j := 0; j < len(marr); j++ {
				l2arr := strings.Split(marr[j], ":")
				if strings.Contains(l2arr[0], "foreignkey") {
					fkeyarr := strings.Split(l2arr[1], ".")
					foreignkey = "FOREIGN KEY(" + fld.Name + ") REFERENCES " + fkeyarr[0] + " (" + fkeyarr[1] + "),"
					keys += foreignkey
				}
				if strings.Contains(l2arr[0], "default") {
					defaultval = "DEFAULT " + l2arr[1]
				} else {
					defaultval = ""
				}
				if strings.Contains(marr[j], "unique") {
					unique = "UNIQUE "
				} else {
					unique = ""
				}
				if strings.Contains(marr[j], "notnull") {
					nullable = "NOT NULL "
				} else if strings.Contains(marr[j], "null") {
					nullable = "NULL "
				} else {
					nullable = ""
				}
				if strings.Contains(marr[j], "key:primary") {
					keys += "PRIMARY KEY(" + fld.Name + "),"
				}
			}
			query += fld.Name + " " + type_sql(fld.Type.String()) + " " + unique + nullable + defaultval + ","
		} else {
			if strings.Contains(string(fld.Tag), "default") {
				defarr := strings.Split(string(fld.Tag), ":")
				defaultval = "DEFAULT " + defarr[1]
			} else {
				defaultval = ""
			}
			if strings.Contains(string(fld.Tag), "foreignkey") {
				fkarr := strings.Split(string(fld.Tag), ":")
				fkey := strings.Split(fkarr[1], ".")
				foreignkey = "FOREIGN KEY(" + fld.Name + ") REFERENCES " + fkey[0] + " (" + fkey[1] + "),"
			}
			if strings.Contains(string(fld.Tag), "unique") {
				unique = "UNIQUE "
			} else {
				unique = ""
			}
			if strings.Contains(string(fld.Tag), "notnull") {
				nullable = "NOT NULL "
			} else if strings.Contains(string(fld.Tag), "null") {
				nullable = "NULL "
			} else {
				nullable = ""
			}
			if strings.Contains(string(fld.Tag), "key:primary") {
				keys += "PRIMARY KEY(" + fld.Name + "),"
			}
			query += fld.Name + " " + type_sql(fld.Type.String()) + " " + unique + nullable + defaultval + ","
		}

	}
	query += keys + ")"
	if strings.Contains(query, ",)") {
		query = query[:len(query)-2] + ")"
	}
	err := base.Run_query(query)
	return query, err
}

// private functions
func ping_db(base *sql.DB) {
	err := base.Ping()
	logger.Print_error(err)
}
func begin_db(base *sql.DB, sBase *Database) {
	tx, err := base.Begin()
	logger.Print_error(err)
	set_transaction(sBase, tx)
}
func set_transaction(base *Database, tran *sql.Tx) {
	base.Transaction = tran
}

func type_sql(val string) string {
	switch val {
	case "int":
		val = "INTEGER"
	case "string":
		val = "TEXT"
	case "time.Time":
		val = "DATE"
	case "float32":
		val = "REAL"
	case "float64":
		val = "REAL" // It's getting pretty real up in here
	case "bool":
		val = "INTEGER"
	case "boolean":
		val = "INTEGER"
	default:
		val = "BLOB"
	}
	return val
}
func new_db_pointer() *Database {
	base := new(Database)
	return base
}
